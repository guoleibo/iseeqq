
import com.sun.jna.Native;

import com.sun.jna.win32.StdCallLibrary;

public class Test
{
	public interface User32 extends StdCallLibrary
	{

		User32 INSTANCE = (User32) Native.loadLibrary("User32", User32.class);// ����ϵͳUser32
		int SendMessageA(int hwnd, int msg, int wparam, int lparam);
		int FindWindowA(String arg0, String arg1);
		void BlockInput(boolean isBlock);
		int MessageBoxA(int hWnd, String lpText, int lpCaption, int uType);
	}

	public static void main(String[] args) throws Exception
	{
		int hwnd = User32.INSTANCE.FindWindowA(null, null);
		System.setProperty("jna.encoding", "GBK");
		User32.INSTANCE.MessageBoxA(hwnd, "PPP", 0, 0);
		int i = 0;
		while (true)
		{
			User32.INSTANCE.BlockInput(true);
			User32.INSTANCE.SendMessageA(hwnd, 0x0112, 0xF170, 2);
			Thread.sleep(2000);
			User32.INSTANCE.SendMessageA(hwnd, 0x0112, 0xF170, -1);
			Thread.sleep(2000);
			i++;
			if (i > 10)
			{
				break;
			}
		}
		User32.INSTANCE.BlockInput(false);

	}
}
