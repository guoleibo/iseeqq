package iseeqq.context;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.junit.Test;

public class QQContextTest {

	@Test
	public void testGetAccount() throws Exception {
		String name = "xiaochao<hey.you@comehereandshowmeyourmoney.com> 2017-11-17 10:38:37";
		
		QQContext qq = new QQContext(null);
		System.out.println(qq.getAccount(name));
	}

	@Test
	public void testGetNickName() throws Exception {
	String name = "xiaochao&lt;hey.you@comehereandshowmeyourmoney.com&gt; 2017-11-17 10:38:37";
		
		QQContext qq = new QQContext(null);
		System.out.println(qq.getNickName(name));
	}

	
	public static void main(String[] args) {
		
		
		String date = "9:52:37";
		LocalDateTime localDateTime = LocalDateTime.of(LocalDate.now(), LocalTime.parse(date,DateTimeFormatter.ofPattern("H:mm:ss")));
		Instant instant = localDateTime.atZone(ZoneId.systemDefault()).toInstant();
		Date from = Date.from(instant);
		System.out.println(from);
	}
}
