package iseeqq.context;

import java.awt.Dimension;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import org.apache.log4j.Logger;

import iseeqq.context.Listener.QQListener;
import iseeqq.img.FingerPrint;
import iseeqq.model.ImgCut;
import iseeqq.tool.ImgUtil;
import iseeqq.tool.QQ;
import iseeqq.tool.ScreenShot;

/**
 * 
 * 
 * @author 戴永杰
 *
 * @date 2017年11月9日 下午4:41:37
 * @version V1.0
 *
 */
public class ISeeQQContext implements Runnable {

	private static Logger logger = Logger.getLogger(ISeeQQContext.class);

	private QQContext qqContext = null;

	private Robot robot;

	private QQListener qqListener;

	public ISeeQQContext(QQContext qqContext) {
		this.qqContext = qqContext;
		this.robot = qqContext.robot;
	}

	public void setQqListener(QQListener qqListener) {
		this.qqListener = qqListener;
	}

	@Override
	public void run() {
		try {
			searchQQPostion(qqContext);
			if (qqContext.isScan() || qqContext.getQqStatus() == 1) { // QQ已启动
				qqContext.setScan(true);
				qqContext.addListerner(qqListener);
			} else {
				logger.debug(qqContext.getQqStatusMsg());
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	public boolean searchQQPostion(QQContext qqContext) throws IOException, InterruptedException {

		if (qqContext.isScan()) {
			return true;
		}

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int h = (int) screenSize.getHeight();
		int w = (int) screenSize.getWidth();
		ImageIcon runProIcon = new Window().getRunProIcon("QQ.exe");
		if (runProIcon == null) {
			qqContext.setQqStatus(-1);
		} else {
			// 保守点，上下边线都减去1px
			BufferedImage bottom = ScreenShot.getScreen(robot, 0, h - 38, w - 80, 35);
			BufferedImage qqIcon = (BufferedImage) runProIcon.getImage();
			BufferedImage kouTuYS = ImgUtil.inverseImage(ImgUtil.custBianyuan(bottom));
			ImgCut[] splitImgX = ImgUtil.shootShadowSimple(kouTuYS).splitImgX();
			for (ImgCut imgCutX : splitImgX) {
			
				ImgCut[] splitImgY = ImgUtil.shootShadowSimple(imgCutX.getImg()).splitImgY();
				for (ImgCut imgCutY : splitImgY) {
					BufferedImage cat = ImgUtil.cat(imgCutX.getX() - 1, imgCutY.getY(), imgCutX.getW() + 2,
							imgCutY.getH(), bottom);
					// 图片大小 10*10~20*20 之间的做比较
					if (cat.getWidth() < 10 || cat.getWidth() > 20 || cat.getHeight() < 10 || cat.getHeight() > 20) {
						continue;
					}
					BufferedImage botPng = ImgUtil.copy(cat);
					botPng = ImgUtil.zoomImage(botPng, qqIcon.getWidth(), qqIcon.getHeight());
					float compare = new FingerPrint(qqIcon).compare(new FingerPrint(botPng));
					if (compare > 0.78) { // 相似度大于80% ，认为找到QQ图片
						imgCutX.setY(h - 38 + imgCutY.getY());
						imgCutX.setH(imgCutY.getH());
						imgCutX.setImg(cat);
						imgCutX.setPid(Window.getQQPid());
						qqContext.setQqPost(imgCutX);
						qqContext.setQqStatus(QQ.getQQStatus(cat));
						logger.debug("ISseeQQ启动成功...");
						return true;
					}
				}
			}
			// 暂时屏蔽
			// BufferedImage bottom2 = ScreenShot.getScreen(robot, 0, h - 39, w
			// - 80, 35);
			// label: for (int i = 0; i < bottom.getHeight(); i++) {
			// for (int j = 0; j < bottom.getWidth(); j++) {
			// if (bottom2.getRGB(j, i) != bottom.getRGB(j, i)) {
			// // 查找点击忽略按钮
			// robot.setAutoDelay(200);
			// robot.mouseMove(j + 3, h - 39 + i + 5);
			// robot.delay(200);
			// robot.mouseMove(j + 2, h - 35 + i);
			// robot.delay(200);
			// robot.mouseMove(j + 2, h - 39 - 25);
			// robot.delay(200);
			// robot.mouseMove(j - 80, h - 39 - 25);
			// robot.delay(200);
			// robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
			// robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
			// robot.delay(200);
			// break label;
			// }
			// }
			// }
			qqContext.setQqStatus(-2);
		}
		return false;
	}
	
	public static void main(String[] args) throws Exception {
		
		BufferedImage wzImage = ImageIO.read(new File("img/bot.png"));
		BufferedImage custBianyuan = ImgUtil.custBianyuan(wzImage);
		Window.outPic(custBianyuan, "img/pp.png");
		
	}

}
